package server.util;

import java.sql.*;

import server.model.players.Client;

/**
 * MySQL Class
 * 
 * @author Ryan / Lmctruck30
 * 
 */

public class MysqlManager {

	/** MySQL Connection */
	public static Connection conn = null;
	public static Statement statement = null;
	public static ResultSet results = null;

	public final static String MYSQLDATABASE = "bts";
	public final static String MYSQLSERVER = "localhost";
	public final static String MYSQLUSERBAME = "root";
	public final static String MYSQLPASSWORD = "btsparabot123";

	/**
	 * Creates a Connection to the MySQL Database
	 */
	public synchronized static void createConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			 conn =
			 DriverManager.getConnection("jdbc:mysql://"+MYSQLSERVER+"/"+MYSQLDATABASE,
			 MYSQLUSERBAME, MYSQLPASSWORD);
			 Misc.println("MySQL Connected");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized static void destroyConnection() {
		try {
			statement.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int loadDonationPoints(Client c) {
		try {
			ResultSet group = statement
					.executeQuery("SELECT * FROM user WHERE username = '"
							+ c.playerName + "'");
			while (group.next()) {
				String groupp = group.getString("usergroupid");
				int mgroup = Integer.parseInt(groupp);
				if (mgroup > 0) {
					return mgroup;
				}
				return 0;
			}
		} catch (Exception e) {
			return -1;
		}
		return -1;
	}

	public static int loadVotingPoints(Client c) {
		try {
			ResultSet group = statement
					.executeQuery("SELECT * FROM user WHERE username = '"
							+ c.playerName + "'");
			while (group.next()) {
				String groupp = group.getString("usergroupid");
				int mgroup = Integer.parseInt(groupp);
				if (mgroup > 0) {
					return mgroup;
				}
				return 0;
			}
		} catch (Exception e) {
			return -1;
		}
		return -1;
	}

	public synchronized static ResultSet query(String s) throws SQLException {
		try {
			if(conn == null || conn.isClosed()) {
				createConnection();
			}
			statement = conn.createStatement();
			if (s.toLowerCase().startsWith("select")) {
				ResultSet rs = statement.executeQuery(s);
				return rs;
			} else {
				statement.executeUpdate(s);
			}

			return null;
		} catch (Exception e) {
			destroyConnection();
			createConnection();
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Save Sessions HighScores
	 * 
	 * @param clientToSave
	 *            The session that saves their stats
	 * @return The flag true if successful
	 */
	public synchronized static boolean saveHighScore(Client clientToSave) {
		try {
			long unixTime = System.currentTimeMillis() / 1000L;
			int week = (int)Math.ceil(unixTime /60.0/60.0/24.0/7.0);
			query("DELETE FROM `skills` WHERE username = '"
					+ clientToSave.playerName + "' AND week="+week+";");
			query("DELETE FROM `skillsoverall` WHERE username = '"
					+ clientToSave.playerName + "' AND week="+week+";");
			query("INSERT INTO `skills` (`username`,`week`,`Attacklvl`,`Attackxp`,`Defencelvl`,`Defencexp`,`Strengthlvl`,`Strengthxp`,`Hitpointslvl`,`Hitpointsxp`,`Rangelvl`,`Rangexp`,`Prayerlvl`,`Prayerxp`,`Magiclvl`,`Magicxp`,`Cookinglvl`,`Cookingxp`,`Woodcuttinglvl`,`Woodcuttingxp`,`Fletchinglvl`,`Fletchingxp`,`Fishinglvl`,`Fishingxp`,`Firemakinglvl`,`Firemakingxp`,`Craftinglvl`,`Craftingxp`,`Smithinglvl`,`Smithingxp`,`Mininglvl`,`Miningxp`,`Herblorelvl`,`Herblorexp`,`Agilitylvl`,`Agilityxp`,`Thievinglvl`,`Thievingxp`,`Slayerlvl`,`Slayerxp`,`Farminglvl`,`Farmingxp`,`Runecraftlvl`,`Runecraftxp`,`Summoninglvl`,`Summoningxp`,`Hunterlvl`,`Hunterxp`,`Constructionlvl`,`Constructionxp`,`Dungeoneeringlvl`,`Dungeoneeringxp`) VALUES ('"
					+ clientToSave.playerName
					+ "',"
					+ week
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[0])
					+ ","
					+ clientToSave.playerXP[0]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[1])
					+ ","
					+ clientToSave.playerXP[1]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[2])
					+ ","
					+ clientToSave.playerXP[2]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[3])
					+ ","
					+ clientToSave.playerXP[3]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[4])
					+ ","
					+ clientToSave.playerXP[4]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[5])
					+ ","
					+ clientToSave.playerXP[5]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[6])
					+ ","
					+ clientToSave.playerXP[6]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[7])
					+ ","
					+ clientToSave.playerXP[7]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[8])
					+ ","
					+ clientToSave.playerXP[8]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[9])
					+ ","
					+ clientToSave.playerXP[9]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[10])
					+ ","
					+ clientToSave.playerXP[10]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[11])
					+ ","
					+ clientToSave.playerXP[11]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[12])
					+ ","
					+ clientToSave.playerXP[12]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[13])
					+ ","
					+ clientToSave.playerXP[13]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[14])
					+ ","
					+ clientToSave.playerXP[14]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[15])
					+ ","
					+ clientToSave.playerXP[15]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[16])
					+ ","
					+ clientToSave.playerXP[16]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[17])
					+ ","
					+ clientToSave.playerXP[17]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[18])
					+ ","
					+ clientToSave.playerXP[18]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[19])
					+ ","
					+ clientToSave.playerXP[19]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[20])
					+ ","
					+ clientToSave.playerXP[20]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[21])
					+ ","
					+ clientToSave.playerXP[21]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[22])
					+ ","
					+ clientToSave.playerXP[22]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[23])
					+ ","
					+ clientToSave.playerXP[23]
					+ ","
					+ clientToSave
					.getLevelForXP(clientToSave.playerXP[24])
					+ ","
					+ clientToSave.playerXP[24]
					+ ");");
			query("INSERT INTO `skillsoverall` (`username`,`week`,`levels`,`xp`) VALUES ('"
					+ clientToSave.playerName
					+ "',"
					+ week
					+ ","
					+ (clientToSave.getLevelForXP(clientToSave.playerXP[0])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[1])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[2])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[3])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[4])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[5])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[6])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[7])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[8])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[9])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[10])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[11])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[12])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[13])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[14])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[15])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[16])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[17])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[18])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[19])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[20])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[21])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[22])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[23])
							+ clientToSave
									.getLevelForXP(clientToSave.playerXP[24]))
					+ ","
					+ ((clientToSave.playerXP[0]) + (clientToSave.playerXP[1])
							+ (clientToSave.playerXP[2])
							+ (clientToSave.playerXP[3])
							+ (clientToSave.playerXP[4])
							+ (clientToSave.playerXP[5])
							+ (clientToSave.playerXP[6])
							+ (clientToSave.playerXP[7])
							+ (clientToSave.playerXP[8])
							+ (clientToSave.playerXP[9])
							+ (clientToSave.playerXP[10])
							+ (clientToSave.playerXP[11])
							+ (clientToSave.playerXP[12])
							+ (clientToSave.playerXP[13])
							+ (clientToSave.playerXP[14])
							+ (clientToSave.playerXP[15])
							+ (clientToSave.playerXP[16])
							+ (clientToSave.playerXP[17])
							+ (clientToSave.playerXP[18])
							+ (clientToSave.playerXP[19])
							+ (clientToSave.playerXP[20])
							+ (clientToSave.playerXP[21])
							+ (clientToSave.playerXP[22])
							+ (clientToSave.playerXP[23])
							+ (clientToSave.playerXP[24]))
					+ ");");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Save Voting Point Info
	 * 
	 * @param c
	 *            The session's client
	 * @return The flag if true was successful
	 */
	public static boolean saveVotingInfo(Client c) {
		try {
			query("INSERT INTO `skills` (`playerName`,`playerPass') VALUES ('"
					+ c.playerName + "'," + c.playerPass + ");");
		} catch (Exception e) {
			// e.printStackTrace();
			return false;
		}
		return true;
	}

}
